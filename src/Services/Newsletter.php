<?php
/**
 * Created by PhpStorm.
 * User: Vali
 * Date: 12.04.2020
 * Time: 12:26
 */

namespace App\Services;


use App\Entity\Post;
use App\Entity\Subscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Templating\EngineInterface;

class Newsletter
{
    /**@var \Swift_Mailer*/
    private $mailer;

    /**@var EngineInterface*/
    private $twig;

    /**@var EntityManagerInterface*/
    private $entityManager;

    /**
     * Newsletter constructor.
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $twig
     * @param $entityManager
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $twig, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    public function sendNewsletter()
    {
        $subscribers = $this->entityManager->getRepository (Subscriber::class)->findAll();

        $lastArticle = $this->entityManager->getRepository (Post::class)->findBy([], ['id'=>'DESC'],1);

        foreach ($subscribers as $subscriber) {


            $message = (new \Swift_Message('Newsletter' . date('Y-m-d')))
                ->setFrom('livia.preda@gmail.com')
                ->setTo($subscriber->getEmail())
                ->setBody($this->twig->renderResponse('default.newsletter.html.twig',
                    [
                        'post' => $lastArticle[0],
                        'subscriber'=>$subscriber
                    ]

                )->getContent(), 'text/html');


            $this->mailer->send($message);
        }
    }
}