<?php

namespace App\Command;

use App\Services\Newsletter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BlogNewsletterCommand extends Command
{
    protected static $defaultName = 'blog:newsletter';

    /**@var \App\Services\Newsletter*/
    protected $newsletterService;

    /**
     * BlogNewsletterCommand constructor.
     * @param Newsletter $newsletterService
     */

    public function __construct(Newsletter $newsletterService)
    {
        $this->newsletterService = $newsletterService;

    parent::__construct();
}


    protected function configure()
    {
        $this->
            setDescription('Sends newsletters!')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

       $this->newsletterService->sendNewsletter();

       $io->success('Newsletter sent successfully!');


        return 0;
    }
}
