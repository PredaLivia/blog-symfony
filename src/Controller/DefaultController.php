<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Subscriber;
use App\Form\PostType;
use App\Form\SubscriberType;
use App\Services\Newsletter;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index( Request $request, PaginatorInterface $paginator)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository(Category::class)->findAll();
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder -> select('p')
                       ->from(Post::class, 'p');
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render('default/index.html.twig', ['categories' =>  $categories, 'pagination' => $pagination,]);
    }

        /**
         * @Route("/post/{id}/{name}", name="post-action")
         */

        public function post ($id, $name)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $post = $entityManager->getRepository(Post::class)->find($id);

            if (is_null($post)) {
                throw new \Exception("A post with id:$id does not exist!");

            }

        } catch (\Exception $ex){
            return $this->render('default/error.html.twig',['errorMessage' => $ex->getMessage()]);
        }

        return $this->render('default/post.html.twig',['post' => $post]);

    }

    /**
     * @Route("/post/adauga", name="post-add-action")
     */

    public function createPost(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $post = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($post);
            $entityManager ->flush();
        }

        return $this->render('default/addPost.html.twig', ['form'=>$form->createView()]);
    }
    /**
     * @Route("/post-edit/{id}", name="post-edit-action")
     */

    public function editPost(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $post = $form->getData();

            $entityManager ->persist($post);
            $entityManager ->flush();
        }

        return $this->render('default/addPost.html.twig', ['form'=>$form->createView()]);
    }


    /**
     * @Route("/subscribe", name="subscribe")
     */

    public function subscribe (Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subscriber = new Subscriber();
         $form = $this->createForm(SubscriberType::class, $subscriber);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $subscriber = $form->getData();

            $entityManager ->persist($subscriber);
            $entityManager ->flush();
        }

return $this->render('default/subscribe.html.twig', ['form'=>$form->createView()]);
}

    /**
     * @Route("/send-newsletters", name="send-newsletters")
     */

    public function sendNewsletters (Request $request, Newsletter $newsletter)
    {

        $newsletter ->sendNewsleter();

        return $this->redirectToRoute('/');
    }
}
